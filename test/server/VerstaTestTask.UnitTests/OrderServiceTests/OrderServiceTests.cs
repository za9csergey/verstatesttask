using FizzWare.NBuilder;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using VerstaTestTask.DataAccessLayer;
using VerstaTestTask.Repository;
using VerstaTestTask.Service;
using Xunit;

namespace VerstaTestTask.OrderServiceTests
{
    public class OrderServiceTests
    {
        private Mock<IRepository<Order>> mock;
        private List<Order> fakeOrders;
        private int orderCount = 30;
        private OrderService service;

        public OrderServiceTests()
        {
            // ������� ��� ��� �����������, �������� ������, ������
            mock = new Mock<IRepository<Order>>();
            fakeOrders = Builder<Order>.CreateListOfSize(orderCount).Build().ToList();
            service = new OrderService(mock.Object);

            // ��������� ���
            Func<int, int, IEnumerable<Order>> predicate = (s, t) => fakeOrders.Skip(s).Take(t);
            mock.Setup(r => r.Get(It.IsAny<int>(), It.IsAny<int>())).Returns(predicate);
            mock.Setup(r => r.Insert(It.IsNotNull<Order>())).Callback<Order>((o) =>fakeOrders.Add(o));
        }

        [Fact]
        public void GetOrders_SkipTakeInRange_ExpectedCount()
        {
            var orders = service.Get(orderCount - 1, 1).ToArray();
            Assert.Equal(1, orders.Count());
            Assert.Equal(orderCount, orders[0].Id);
        }

        [Fact]
        public void GetOrders_TakeGreatherCount_ExpectedCount()
        {
            var orders = service.Get(0, orderCount + 10).ToArray();
            Assert.Equal(orderCount, orders.Count());
        }

        [Theory]
        [InlineData(-1, 10)]
        [InlineData(1, 0)]
        [InlineData(1, -1)]
        public void GetOrders_InvalidSkipTake_ArgumentException(int skip, int take)
        {
            Assert.Throws<ArgumentException>(() => service.Get(skip, take));
        }

        [Fact]
        public void InsertOrder_ValidOrder_OrderInserted()
        {
            Order order = Builder<Order>.CreateNew().Build();
            service.Insert(order);
            Assert.Equal(orderCount + 1, fakeOrders.Count);

            //Tear down
            fakeOrders.Remove(order);
        }

        [Fact]
        public void InsertOrder_OrderIsNull_ArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => service.Insert(null));
        }
    }
}
