﻿using Microsoft.EntityFrameworkCore;
using VerstaTestTask.DataAccessLayer;

namespace VerstaTestTask.Repository
{
    /// <summary>
    /// Контекст хранилища заказов
    /// </summary>
    public class OrderContext : DbContext
    {
        public OrderContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            new OrderMap(modelBuilder.Entity<Order>());
        }

    }
}
