﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using VerstaTestTask.DataAccessLayer;

namespace VerstaTestTask.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly OrderContext orderContext;
        private DbSet<T> items;

        public Repository(OrderContext orderContext)
        {
            this.orderContext = orderContext;
            items = orderContext.Set<T>();
        }
        
        /// <summary>
        /// Получить список объектов репозитория
        /// </summary>
        /// <param name="skip">Количество объектов, которые пропускаем</param>
        /// <param name="take">Количество объеов, которые возращаем</param>
        /// <returns>Список объектов репозитория</returns>
        public IEnumerable<T> Get(int skip, int take)
        {
            return items.Skip(skip).Take(take).AsEnumerable();
        }

        /// <summary>
        /// Количество объектов в репозитории
        /// </summary>
        /// <returns>Количество объектов репозитории</returns>
        public int GetItemsCount()
        {
            return items.Count();
        }

        /// <summary>
        /// Добавить в репозитоий новый объект
        /// </summary> 
        /// <param name="item">Объект дял добавления в репозиторий</param>
        public void Insert(T item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("entity");
            }
            items.Add(item);
            orderContext.SaveChanges();

        }

        /// <summary>
        /// Сохранение измений в репозитории
        /// </summary>
        public void SaveChanges()
        {
            orderContext.SaveChanges();
        }
    }
}
