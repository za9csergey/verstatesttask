﻿using System.Collections.Generic;

namespace VerstaTestTask.Repository
{
    /// <summary>
    /// Интерфейс для репозитория
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRepository<T> where T : class
    {
        /// <summary>
        /// Получить список объектов репозитория
        /// </summary>
        /// <param name="skip">Количество объектов, которые пропускаем</param>
        /// <param name="take">Количество объеов, которые возращаем</param>
        /// <returns>Список объектов репозитория</returns>
        IEnumerable<T> Get(int skip, int take);

        /// <summary>
        /// Количество объектов в репозитории
        /// </summary>
        /// <returns>Количество объектов репозитории</returns>
        int GetItemsCount();

        /// <summary>
        /// Добавить в репозитоий новый объект
        /// </summary> 
        /// <param name="item">Объект дял добавления в репозиторий</param>
        void Insert(T item);

        /// <summary>
        /// Сохранение измений в репозитории
        /// </summary>
        void SaveChanges();
    }
}