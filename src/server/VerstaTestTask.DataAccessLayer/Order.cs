﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VerstaTestTask.DataAccessLayer
{
    /// <summary>
    /// Заказ
    /// </summary>
    public class Order
    {
        /// <summary>
        /// Идентификатор заказа
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Город отправления
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string CityDepature { get; set; }

        /// <summary>
        /// Адрес отправления
        // </summary>
        [Required]
        public string AddressDepature { get; set; }

        /// <summary>
        /// Город назначения
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string CityDestination { get; set; }

        /// <summary>
        /// Адрес назначения
        // </summary>
        [Required]
        [MaxLength(50)]
        public string AddressDestination { get; set; }

        /// <summary>
        /// Вес груза в кг
        /// </summary>
        [Required]
        public double CargoWeight { get; set; }

        /// <summary>
        /// Время отправление заказа
        /// </summary>
        [Required]
        public DateTime CargoDateTime { get; set; }
    }
}