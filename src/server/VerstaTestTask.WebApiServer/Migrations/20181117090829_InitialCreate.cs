﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VerstaTestTask.WebApiServer.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Order",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CityDepature = table.Column<string>(maxLength: 50, nullable: false),
                    AddressDepature = table.Column<string>(nullable: false),
                    CityDestination = table.Column<string>(maxLength: 50, nullable: false),
                    AddressDestination = table.Column<string>(maxLength: 50, nullable: false),
                    CargoWeight = table.Column<double>(nullable: false),
                    CargoDateTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Order");
        }
    }
}
