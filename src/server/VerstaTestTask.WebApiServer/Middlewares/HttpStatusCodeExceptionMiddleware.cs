﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace VerstaTestTask.WebApiServer.Middlewares
{
    /// <summary>
    /// Промежуточный слой для обработки исключений
    /// </summary>
    public class HttpStatusCodeExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public HttpStatusCodeExceptionMiddleware(RequestDelegate next)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (ArgumentNullException ane)
            {
                // Bad request
                context.Response.StatusCode = 400;
                await context.Response.WriteAsync(
                    $"400 Bad request.\n{ane.ParamName}: {ane.Message}");
            }
            catch (ArgumentException ae)
            {
                // Bad request
                context.Response.StatusCode = 400;
                await context.Response.WriteAsync(
                    $"400 Bad request.\n{ae.ParamName}: {ae.Message}");
            }
            catch (Exception ex)
            {
                // Server error
                context.Response.StatusCode = 500;
                await context.Response.WriteAsync($"500 Server error.\n {ex.Message}");
            }
        }
    }
}