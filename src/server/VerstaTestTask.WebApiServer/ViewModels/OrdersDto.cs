﻿using System.Collections.Generic;
using VerstaTestTask.DataAccessLayer;

namespace VerstaTestTask.WebApiServer.ViewModels
{
    public class OrdersDto
    {
        public IEnumerable<Order> Orders { get; }
        public int TotalOrderCount { get; }

        public OrdersDto(IEnumerable<Order> orders, int totalOrderCount)
        {
            Orders = orders;
            TotalOrderCount = totalOrderCount;
        }
    }
}