﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VerstaTestTask.DataAccessLayer;
using VerstaTestTask.Service;
using VerstaTestTask.WebApiServer.ViewModels;

namespace VerstaTestTask.WebApiServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
	
    public class OrdersController : ControllerBase
    {
        private readonly IOrderService orderService;

        public OrdersController(IOrderService orderService)
        {
            this.orderService = orderService;
        }

        // GET: api/Orders?skip=10&take=5
        [HttpGet]
        public OrdersDto Get([FromQuery]int? skip, [FromQuery]int? take)
        {
			if (!skip.HasValue){
				skip = 0;
			}
			if (!take.HasValue){
				take = 10;
			}
            // validate
            if (skip < 0)
            {
                throw new ArgumentException($"skip is negative: {skip}",
                    nameof(skip));
            }
            if (take <= 0)
            {
                throw new ArgumentException($"take isn't positive: {skip}",
                    nameof(take));
            }

            return new OrdersDto(orderService.Get(skip.Value, take.Value), orderService.GetOrderTotalCount());
        }

        // POST: api/Orders
        [HttpPost]
        public IActionResult Post([FromBody] Order order)
        {
            if (order == null)
            {
                throw new ArgumentException("Some of fields didn't fill");
            }

            orderService.Insert(order);
            return Created(Uri.UriSchemeHttps, order);
        }

        // PUT: api/Orders/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] string value)
        {
            return StatusCode(405);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
