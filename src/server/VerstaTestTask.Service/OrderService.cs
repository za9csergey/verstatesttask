﻿using System;
using System.Collections.Generic;
using VerstaTestTask.DataAccessLayer;
using VerstaTestTask.Repository;

namespace VerstaTestTask.Service
{
    /// <summary>
    /// Работа с заказами
    /// </summary>
    public class OrderService : IOrderService
    {
        private readonly IRepository<Order> repository;

        public OrderService(IRepository<Order> repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Коллецкия заказов 
        /// </summary>
        /// <param name="skip">Сколько заказов пропустить</param>
        /// <param name="take">Сколько заказов вернуть</param>
        /// <returns>Коллекция заказов</returns>
        public IEnumerable<Order> Get(int skip, int take)
        {
            // validate
            if (skip < 0)
            {
                throw new ArgumentException($"skip is negative: {skip}",
                    nameof(skip));
            }
            if (take <= 0)
            {
                throw new ArgumentException($"take isn't positive: {skip}",
                    nameof(take));
            }

            // возвращаем заказы из репозитория
            return repository.Get(skip, take);
        }

        /// <summary>
        /// Количество заказов
        /// </summary>
        /// <returns>Количество заказов</returns>
        public int GetOrderTotalCount()
        {
            return repository.GetItemsCount();
        }

        /// <summary>
        /// Создать новый заказ
        /// </summary>
        /// <param name="newOrder">Сущность нового заказа</param>
        public void Insert(Order newOrder)
        {
            // validate
            if (newOrder == null)
            {
                throw new ArgumentNullException(nameof(newOrder));
            }

            // добавляем заказы в репозиторий
            repository.Insert(newOrder);
            repository.SaveChanges();
        }
    }
}