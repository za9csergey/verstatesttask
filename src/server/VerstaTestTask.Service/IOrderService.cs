﻿using System.Collections.Generic;
using VerstaTestTask.DataAccessLayer;

namespace VerstaTestTask.Service
{

    public interface IOrderService
    {
        /// <summary>
        /// Коллецкия заказов 
        /// </summary>
        /// <param name="skip">Сколько заказов пропустить</param>
        /// <param name="take">Сколько заказов вернуть</param>
        /// <returns>Коллекция заказов</returns>
        IEnumerable<Order> Get(int skip, int take);

        /// <summary>
        /// Количество заказов
        /// </summary>
        /// <returns>Количество заказов</returns>
        int GetOrderTotalCount();

        /// <summary>
        /// Создать новый заказ
        /// </summary>
        /// <param name="newOrder">Сущность нового заказа</param>
        void Insert(Order newOrder);
    }
}