import React from 'react'
import Row from './TableRow';

class TableHeader extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isHeader: true,
            columns: this.props.columns,
            headClass: this.props.headClass,
            rowClass: this.props.rowClass,
            cellClass: this.props.cellClass
        };
    }

    render(){       
        return(
            <React.Fragment>
                <Row
                    className={this.state.rowClass}
                    cellClass={this.state.cellClass}
                    row={this.state.columns}
                    isHeader={this.state.isHeader} />
                
            </React.Fragment>
        )
    }
}

export default TableHeader;