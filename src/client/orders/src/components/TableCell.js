import React from 'react'

class TableCell extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isHeader: this.props.isHeader,
            cell: this.props.cell,
            className: this.props.class
        };
    }

    render(){
        if (this.state.isHeader === true){
            return(
                <th className={this.state.className}>{this.state.cell}</th>
            )    
        }
        else{
            return(
                <td className={this.state.className}>{this.state.cell}</td>
            )        
        }        
    }
}

export default TableCell;