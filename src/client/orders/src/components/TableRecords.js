import React from 'react'
import Row from './TableRow';

class TableRecords extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            isHeader: false,
            data: this.props.data,
            bodyClass: this.props.bodyClass, 
            rowClass: this.props.rowClass,
            cellClass: this.props.cellClass
        };
    }

    render(){
        console.log('records render');
        console.log(this.state.data);
        const data = this.state.data;
        return(
            <React.Fragment>
               {data.map((row) =>
                    <Row
                        className={this.state.rowClass}
                        cellClass={this.state.cellClass}
                        row={row}
                        isHeader={this.isHeader} />
                )}
                
            </React.Fragment>
        )
    }
}

export default TableRecords;