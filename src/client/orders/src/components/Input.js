import React from 'react';

const Input = props => {
    //console.log(props.value);
    return (
      <div className="form-group">
        <label for={props.name} className="form-label">
          {props.title}
        </label>
        <input
          className="form-control"
          step='1'
          id={props.name}
          name={props.name}
          type={props.inputType}
          value={props.value}
          onChange={props.handleChange}
          placeholder={props.placeholder}
          onfocusout={props.validateNumber}
          {...props}
        />
      </div>
    );
  };
  
  export default Input;