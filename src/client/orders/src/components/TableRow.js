import React from 'react';
import TableCell from './TableCell';

class Row extends React.Component{
    constructor(props){
        super(props);        
        this.state = {
            isHeader: this.props.isHeader,
            row: this.props.row,
            className: this.props.className,
            cellClass: this.props.cellClass
        };
    }

    render(){
        
        const row = this.state.row;        
        return(
            <React.Fragment>
                <tr>
                {Object.keys(row).map((key)=>
                    <TableCell 
                        isHeader={this.state.isHeader}
                        cell={this.state.row[key]}
                        className={this.state.cellClass} />
                )}
                </tr>
            </React.Fragment>
        )
    }
}

export default Row;