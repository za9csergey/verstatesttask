import React from 'react';

class Message extends React.Component{    
    render(){
        console.log(this.props);
        let errorMessage = [];
        if (Array.isArray(this.props.errorMessage) !== true){
            errorMessage[0] = this.props.errorMessage;
        }
        else{
            errorMessage = [...this.props.errorMessage];
        }
        return[
            <div id='messageContainer'>
                <div>
                    <h4>{this.props.message}</h4>
                </div>
                <div>
                    {                    
                        errorMessage.map((item)=>
                        <div>{item}</div>
                    )}
                </div>
            </div>
        ]
        
        
    }
}

export default Message;