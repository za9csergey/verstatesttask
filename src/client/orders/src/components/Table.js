import React from 'react'
import TableHeader from './TableHeader.js';
import TableRecords from './TableRecords.js';

class Table extends React.Component{
    constructor(props){
        super(props);
        console.log('table ctor props')
        console.log(this.props.data)       
    }

    render(){
        
        return(
            <React.Fragment>
                <table class={this.props.className}>
                    <thead>
                        <TableHeader
                            columns={this.props.columns}
                            headClass='thead'
                            rowClass='headerRow'
                            cellClass='headerCell' />
                    </thead>
                    <tbody>                    
                        <TableRecords
                            data={this.props.data}
                            bodyClass='data'
                            rowClass='dataRow'
                            cellClass='dataCell' />
                    </tbody>
                </table>
            </React.Fragment>
        )
    }
}

export default Table;