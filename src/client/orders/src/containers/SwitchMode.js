import React from 'react';
import ReactDOM from 'react-dom';
import ShowOrders from './ShowOrders';
import CreateOrder from './CreateOrder';

const text = ['Создать заказ', 'Список заказов'];

const stateContent = ['create', 'show'];

const content = [CreateOrder, ShowOrders];

// компонет для смены контента - список заказов/создание заказов 
class SwitchMode extends React.Component{
    state = {
        mode: stateContent[0],
        showText: text[0],
        renderContent: CreateOrder
    }

    handleClick = () => {
        console.log(this.state.renderContent);
        // рендерим контент
        ReactDOM.render(
            <this.state.renderContent />,
            document.getElementById('root')
        );

        // обновляем state дял рендера следующего компонента
        let i = 0;
        if(this.state.mode === stateContent[0]){
            i = 1;
        }

        this.setState({
            mode : stateContent[i],
            showText: text[i],
            renderContent: content[i]
        });
        
    }

    render(){
        return (
            <div>
                <button className="btn btn-link" onClick={this.handleClick}>{this.state.showText}</button>
            </div>
        )
    }
}

export default SwitchMode;