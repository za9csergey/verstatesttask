import React from 'react';
import axios from 'axios';
import Table from '../components/Table';
import Message from '../components/Message';

let skip = 0;
const take = 5;
const api = "https://localhost:5001/api/orders";
let firstLoad = true;

let columns = [
    'ID', 'Город отправителя', 'Адрес отправителя', 'Город получателя', 
    'Адрес получателя', 'Вес груза (кг)', 'Дата забора груза'
];

// комнонент для вывода списка заказов
class ShowOrders extends React.Component{
    constructor(){
        super();
        // определяем стэйт
        this.state ={
            orders: [{
                id: '',
                cityDepature: '',
                addressDepature: '',
                cityDestination: '',
                addressDestination: '',
                cargoWeight: '',
                cargoDateTime: ''
            }],
            totalOrderCount: '',
            status:{
                message: '',
                errorMessage: ''
            }
        };
        // bind обработчиков
        this.handlePrevPageClick = this.handlePrevPageClick.bind(this);
        this.handleNextPageClick = this.handleNextPageClick.bind(this);
        this.loadData = this.loadData.bind(this);
        this.setPageButtonsVisibility = this.setPageButtonsVisibility.bind(this);
    }

    // нажатие кнопки Предыдущие
    handlePrevPageClick(){
        skip -= take;
        // определяем состояние кнопок
        document.getElementById('nextPageButton').disabled = false;
        if (skip - take < 0){
            document.getElementById('prevPageButton').disabled = true;
        }
        // загружаем заказы
        this.loadData();
    }

    // нажатие кнопки Следующие
    handleNextPageClick(){
        skip += take;
        // загружаем заказы
        this.loadData();
        // определяем состояние кнопок
        document.getElementById('prevPageButton').disabled = false;
        if (skip + take >= this.state.totalOrderCount){
            document.getElementById('nextPageButton').disabled = true;
        }
    }

    // определяем вдимость кнопок
    setPageButtonsVisibility(visibilty){
        document.getElementById('prevPageButton').style.visibility = visibilty;
        document.getElementById('nextPageButton').style.visibility = visibilty;
    }

    // загрузка заказов
    loadData(){
        // отправляем GET request
        axios.get(api+'?skip='+skip+'&take='+take)
            .then((response) => {
                let orders = response.data.orders;
                // если заказов нет, выводим сообщение
                if (orders.length === 0){
                    this.setState({
                        status:{
                            isOk: false,
                            message: 'Список заказов пуст',
                            errorMessage: ''
                        }
                    });
                    document.getElementById('messageContainer').style.visibility = 'visible';
                    document.getElementById('ordersContainer').remove();

                    
                    return false;
                }

                // приводим дату к удобному формату
                orders.map((order) => {
                    let d = new Date(order.cargoDateTime);
                    order.cargoDateTime = d.toLocaleString().replace(',', '');
                })

                // 'обновляем' state
                this.setState({
                    orders: orders,                    
                    totalOrderCount: response.data.totalOrderCount
                });
                // определяем видимость кнопок
                if (firstLoad === true){
                    document.getElementById('prevPageButton').disabled = true;
                    if(skip + take >= this.state.totalOrderCount){
                        this.setPageButtonsVisibility('hidden');                    
                    }
                    else{
                        this.setPageButtonsVisibility('visible');
                    }
                    firstLoad = false;
                    return false;
                }     
            })
            .catch((error) => {
                document.getElementById('messageContainer').style.visibility = 'visible';
                document.getElementById('ordersContainer').remove();
                                
                this.setState({
                    status:{
                        isOk: false,
                        message: 'Bad request',
                        errorMessage: error.response.data
                    }
                });
                
            });
             
    }

    // загружаем заказы когда компонент привязан
    componentDidMount(){
        this.loadData();           
    }

    render(){      
        return (
            <React.Fragment>
                <div id='ordersContainer'>
                    <h3>Список заказов</h3>
                    {/* <Table columns={columns} data={orders} className='table table-bordered'/> */}
                    {/* список заказов*/}
                    <table className='table table-bordered table-striped'>
                        {/*заголовок*/}
                        <thead>
                            <tr>
                                {columns.map((value)=>
                                    <th>{value}</th>
                                )}
                            </tr>
                        </thead>
                        {/*данные*/}
                        <tbody>
                            {this.state.orders.map((row)=>
                                <tr>
                                    {/*строим ячейки*/}
                                    {Object.keys(row).map((key)=>
                                        <td>{row[key]}</td>    
                                    )}
                                </tr>
                            )}
                        </tbody>
                    </table>
                    {/*Paging*/}
                    <div>
                        <div id='prevButtonDiv' className='firstButton'>
                            <button 
                                id="prevPageButton"
                                class='btn btn-primary' 
                                onClick={this.handlePrevPageClick}>Предыдущие {take}</button>
                        </div>
                        <div id='nextButtonDiv' className='secondButton'>
                            <button 
                                id="nextPageButton" 
                                class='btn btn-success'
                                onClick={this.handleNextPageClick}>Следующие {take} ({this.state.totalOrderCount})</button>
                        </div>
                    </div>
                </div>
                {/*Message*/}
                <Message 
                    message={this.state.status.message}
                    errorMessage={this.state.status.errorMessage}
                    />
            </React.Fragment>            
        )
    }
}

export default ShowOrders;