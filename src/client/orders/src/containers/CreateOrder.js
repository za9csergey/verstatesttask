import React from 'react';
import Input from '../components/Input';
import axios from 'axios';
import Message from '../components/Message'

const api = "https://localhost:5001/api/orders";
const orderTitles = {
    cityDepature: 'Город отправления',
    addressDepature: 'Адрес отправления',
    cityDestination: 'Город получателя',
    addressDestination: 'Адрес получателя',
    cargoWeight: 'Вес груза (кг)',
    cargoDateTime: 'Дата забора груза'
}


// Компонент для создания нового заказа
class CreateOrder extends React.Component{
    constructor(props) {
        super(props);
        // определяем state
        this.state = {
            newOrder: {
                cityDepature: '',
                addressDepature: '',
                cityDestination: '',
                addressDestination: '',
                cargoWeight: '',
                cargoDateTime: ''
            },
            status: {
                message: '',
                errorMessage: ''
            }
        };

        // bind обработчиков
        this.handleText = this.handleText.bind(this);
        this.handleSubmitForm = this.handleSubmitForm.bind(this);
        this.handleClearForm = this.handleClearForm.bind(this);
        this.setStatus = this.setStatus.bind(this);
    }

    // создаем заказ
    handleSubmitForm(e){
        e.preventDefault();
        // получаем state
        let order = this.state.newOrder;
        // валидация
        const errorMessage = this.validateOrder(order);
        if (errorMessage.length !== 0){
            const eMessage = 'Необходимо заполнить поля: ' + errorMessage.join(',');
            this.setStatus('Ошибка!', eMessage);
            return false;
        }
        // сброс статуса
        this.setStatus();

        // post request to api
        axios.post(api, order).
            then((respose) =>{
                // очищаем данные и обновляем статус
                this.clearForm();
                this.setStatus('Заказ создан!', '');                
                
            })
            .catch((error) => {
                console.log(error.response.data);
                // составляем текст ошибки от сервиса
                const errorMessage = this.processError(error.response.data);
                this.setStatus('Ошибка!', errorMessage);
            });
    }

    validateOrder(order){
        let errormessage = [];
        // проверка на пустые строки
        Object.keys(order).map((key) => {
            const item = order[key];
            if(!item || !item.trim())
            {   
                errormessage.push(orderTitles[key]);
            }
        });
        return errormessage;
    }

    // составление текста ошибки от сервиса
    processError(data){
        // если несколько параметр
        let result = [];
        
        Object.keys(data).map((key) => {
            console.log(key);
            let item = data[key];
            item.forEach(element => {
                result.push(element);    
            });            
        });       
        
        console.log(result);
        return result;
    }

    setStatus(message, errorMessage) {
        console.log(this.state);
        const prevState = this.state;
        this.setState({
            newOrder: {
                ...prevState.newOrder,
            },
            status: {
                message: message,
                errorMessage: errorMessage
            }
        });
    }

    // очищаем форму
    handleClearForm(e){
        e.preventDefault();
        this.clearForm();
    }

    // очистка формы
    clearForm(){
        // сбрасываем state
        this.setState({
            newOrder: {
                cityDepature: '',
                addressDepature: '',
                cityDestination: '',
                addressDestination: '',
                cargoWeight: '',
                cargoDateTime: ''
            },
            status: {
                message: '',
                errorMessage: ''
            }        
        });
    }

    // обработка текстовых полей]
    handleText(e) {
        let value = e.target.value;
        let name = e.target.name;
        // 'обновляем' state
        this.setState(
            prevState => ({
                newOrder: {
                ...prevState.newOrder,
                [name]: value
                },
                status: {
                    message: '',
                    errorMessage: ''
                }
            })
        );
    }
    
    render(){
        return (
            <React.Fragment>
                {/*форма*/}
                <form id="newOrderForm" onSubmit={this.handleSubmitForm}>
                    {/*тестовые поля*/}
                    <Input
                        inputType={"text"}
                        title={orderTitles.cityDepature}
                        name={"cityDepature"}
                        value={this.state.newOrder.cityDepature}
                        placeholder={"Введите название города"}
                        handleChange={this.handleText}
                    />{" "}
                    <Input
                        inputType={"text"}
                        title={orderTitles.addressDepature}
                        name={"addressDepature"}
                        value={this.state.newOrder.addressDepature}
                        placeholder={"Введите адрес"}
                        handleChange={this.handleText}
                    />{" "}
                    <Input
                        inputType={"text"}
                        title={orderTitles.cityDestination}
                        name={"cityDestination"}
                        value={this.state.newOrder.cityDestination}
                        placeholder={"Введите название города"}
                        handleChange={this.handleText}
                    />{" "}
                    <Input
                        inputType={"text"}
                        title={orderTitles.addressDestination}
                        name={"addressDestination"}
                        value={this.state.newOrder.addressDestination}
                        placeholder={"Введите адрес"}
                        handleChange={this.handleText}
                    />{" "}
                    {/*вес груза*/}
                    <Input
                        inputType={"number"}
                        title={orderTitles.cargoWeight}
                        name={"cargoWeight"}
                        value={this.state.newOrder.cargoWeight}
                        placeholder={"0"}
                        handleChange={this.handleText}
                    />{" "}
                    {/*Дата отгрузки*/}
                    <Input
                        inputType={"datetime-local"}
                        title={orderTitles.cargoDateTime}
                        name={"cargoDateTime"}
                        value={this.state.newOrder.cargoDateTime}
                        placeholder={""}
                        handleChange={this.handleText}
                    />{" "}    
                </form>
                
                {/*отправка и очистка формы*/}
                <div>
                    <div id='submitButtonDiv' className='firstButton'>
                        <button 
                            inputType='submit'
                            id="submitButton"
                            class='btn btn-primary' 
                            onClick={this.handleSubmitForm}>Создать</button>
                    </div>
                    <div id='clearButtonDiv' className='secondButton'>
                        <button 
                            id="clearButton" 
                            class='btn btn-secondary'
                            onClick={this.handleClearForm}>Очистить форму</button>
                    </div>
                    <Message 
                        isOk={this.state.status.isOk}
                        message={this.state.status.message}
                        errorMessage={this.state.status.errorMessage}
                    />
                </div>
            </React.Fragment>
        )
    }
}

export default CreateOrder;