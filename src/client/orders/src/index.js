import React from 'react';
import ReactDOM from 'react-dom';
import CreateOrder from './containers/CreateOrder.js';
import ShowOrders from './containers/ShowOrders.js';
import SwitchMode from "./containers/SwitchMode.js"


ReactDOM.render(
    <SwitchMode />,
    document.getElementById('switchDiv')
);

ReactDOM.render(
    <ShowOrders />,
    document.getElementById('root')
);
